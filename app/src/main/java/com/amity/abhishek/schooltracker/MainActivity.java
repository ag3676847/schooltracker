package com.amity.abhishek.schooltracker;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    public void getUserID(){
        Intent intent=getIntent();
        String userid=intent.getStringExtra("User Id");
    }
    private static final int PERMISSIONS_REQUEST = 1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button start = findViewById(R.id.startTracking);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
                if (!Objects.requireNonNull(lm).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    Toast.makeText(MainActivity.this, "Please enable location services", Toast.LENGTH_SHORT).show();
                    finish();
                }

                // Check location permission is granted - if it is, start
                // the service, otherwise request the permission
                int permission = ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permission == PackageManager.PERMISSION_GRANTED) {
                    startTrackerService();
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            PERMISSIONS_REQUEST);
                }
            }
        });
        // Check GPS is enabled

    }

    private void logout() {
        FirebaseAuth.getInstance().signOut();
        Intent intent= new Intent(this,SignInActivity.class);
        startActivity(intent);
    }
    private void startTrackerService() {
        Intent intent=new Intent(this, SchoolService.class);
        intent.putExtra("TransID", ((EditText)findViewById(R.id.busno)).getText().toString());
        startService(intent);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[]
            grantResults) {
        if (requestCode == PERMISSIONS_REQUEST && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Start the service when the permission is granted
            startTrackerService();
        } else {
            finish();
        }
    }
}
